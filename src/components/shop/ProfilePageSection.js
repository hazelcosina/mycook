import ProductsContext from "contexts/ProductsContext.js";
import React, { useContext, useState } from 'react';
import {
  NavItem,
  NavLink,
  Nav,
  TabContent,
  Container,
  Row,
  Col
} from "reactstrap";
import ProductsTab from "./ProductsTab.js";
import ReviewsTab from "./ReviewsTab.js"

const ProfilePageSection = (props) => {

  const { shop } = useContext(ProductsContext);
  const [activeTab, setActiveTab] = useState("2");

  const toggle = (tab) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  }

  return (
    <div className="section profile-content">
      <Container>
        <div className="owner">
          < br />
          <div className="name">
            <h6 className="description">{shop.shopName}
              <span className="text-warning">&nbsp;(4/5 Rating)</span></h6>
          </div>
        </div>

        <Row>
          <Col className="ml-auto mr-auto text-center" md="6">
            <p>
              {shop.description}
            </p>
            <label className="label label-default mr-1">
              {shop.address} , {shop.city}
            </label >
            <br />
          </Col>
        </Row>
        <div className="nav-tabs-navigation">
          <div className="nav-tabs-wrapper">
            <Nav role="tablist" tabs>
              <NavItem>
              </NavItem>
              <NavItem>
                <NavLink
                  className={activeTab === "2" ? "active" : ""}
                  onClick={() => {
                    toggle("2");
                  }} >
                  menu&nbsp;
                    <small className="nc-icon nc-favourite-28 align-middle text-danger " />
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={activeTab === "3" ? "active" : ""}
                  onClick={() => {
                    toggle("3");
                  }}
                >
                  reviews&nbsp;

                  </NavLink>
              </NavItem>
            </Nav>
          </div>
        </div>
        {/* Tab panes */}
        <TabContent className="reviews" activeTab={activeTab}>
          <ProductsTab list={shop.productList} />
          <ReviewsTab />
        </TabContent>
      </Container>
    </div>
  )

}
export default ProfilePageSection;
