import React from "react";

// reactstrap components
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  CardImg,
  Table,
  Modal,
  ModalBody

} from "reactstrap";

const ReviewsTab = ({ list }) => {
  const [modal, setModal] = React.useState(false);
  const [product, selectedReview] = React.useState({});

  const toggleModal = (review) => {
    setModal(!modal);
    selectedReview(review);
  };
  return (
    <TabPane tabId="3" id="reviews">
      {/* <h3 className="text-muted">No reviews from anyone yet :(</h3>
        <Button className="btn-round" color="warning">
          Go back
          </Button> */}
      {/* <h5 className="text-muted">Customer's Rating:  4/5
           &nbsp;<i className="nc-icon nc-favourite-28 align-middle text-danger " />
        <i className="nc-icon nc-favourite-28 align-middle text-danger " />
        <i className="nc-icon nc-favourite-28 align-middle text-danger " />
        <spian className="nc-icon nc-favourite-28 align-middle text-danger " />
        <i className="nc-icon nc-favourite-28 align-middle" />
      </h5> */}

      <Row>

        <Col className="ml-auto mr-auto" md="12">
          <Modal isOpen={modal} toggle={toggleModal}>
            <div className="modal-header">
              <button
                aria-label="Close"
                className="close"
                type="button"
                onClick={toggleModal}
              >
                <span aria-hidden={true}>×</span>
              </button>
              <h5 className="modal-title text-center"
                id="exampleModalLabel">
                By: Brad P.
              </h5>
            </div>
            <img width="100%" src={require("assets/img/examples/cake1.jpg")} alt="Card image cap" />

            <ModalBody >
              <h5>(5/5 Rating)</h5>
<small> They offer such beautiful cakes. My cousin is very happy with it. Aside from the presentation, it literally tasted good, especially chocolate and red velvet flavored.
  Will definetly order again! </small>
            </ModalBody>
          </Modal>

          <ul className="list-unstyled listing">
            <li onClick={toggleModal}>
              <Row>
                <Col className="ml-auto mr-auto" lg="2" md="4" xs="6">
                  <img
                    alt="..."
                    className="img-fluid"
                    src={require("assets/img/examples/cake/review1.jpg")} />
                </Col>
                <Col className="ml-auto mr-auto" lg="10" md="8" xs="6">
                  <small> By: Brad P. </small> &nbsp;(5/5 Rating) <br />
                  <small>
                  They offer such beautiful cakes. My cousin is very happy with it. Aside from the presentation, it literally tasted good, especially chocolate and red velvet flavored.
  Will definetly order again!                   </small>
                      <br />

                </Col>
              </Row>
              <hr/>
            </li>

            <li onClick={toggleModal}>
              <Row>
                <Col className="ml-auto mr-auto" lg="2" md="4" xs="6">
                  <img
                    alt="..."
                    className="img-fluid"
                    src={require("assets/img/examples/cake/review2.jpg")} />
                </Col>
                <Col className="ml-auto mr-auto" lg="10" md="8" xs="6">
                  <small> By: Katy P. </small> &nbsp;(4/5 Rating)<br />
                  <small>
                  I love the icing. It's not too sweet, just the right amount of sweetness. Will recommend to my friends!  </small>
                  <br />
                </Col>
              </Row>
              <hr />
            </li>

          </ul>
        </Col>
      </Row>
    </TabPane>
  )
}

export default ReviewsTab;