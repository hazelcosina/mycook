import React from "react";

// reactstrap components
import {
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  CardImg,
  Table,
  Modal,
  ModalBody

} from "reactstrap";

const GalleryTab = ({ list }) => {
  const [modal, setModal] = React.useState(false);
  const [product, selectedReview] = React.useState({});

  const toggleModal = (review) => {
    setModal(!modal);
    selectedReview(review);
  };
  return (
    <TabPane tabId="1" id="gallery">
      <Modal isOpen={modal} toggle={toggleModal}>
        <div className="modal-header">
          <button
            aria-label="Close"
            className="close"
            type="button"
            onClick={toggleModal}
          >
            <span aria-hidden={true}>×</span>
          </button>
          <h5 className="modal-title text-center"
            id="exampleModalLabel">
            By: Brad P.
              </h5>
        </div>
        <img width="100%" src={require("assets/img/examples/sample8.jpg")} alt="Card image cap" />

        <ModalBody >
          <h5>(5/5 Rating)</h5>
          <small> They offer such beautiful cakes. My cousin is very happy with it. Aside from the presentation, it literally tasted good, especially chocolate and red velvet flavored.
  Will definetly order again! </small>
        </ModalBody>
      </Modal>
      <Row className="no-gutters">
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake1.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake2.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake3.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake4.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake5.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/cake/cake6.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/sample7.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/sample8.jpg")} />
        </Col>
        <Col lg="4" md="4" xs="4">
          <img
            alt="..."
            className="img-fluid"
            src={require("assets/img/examples/smple3.jpg")} />
        </Col>
      </Row>
    </TabPane>
  )
}

export default GalleryTab;