import React, { useContext } from 'react';

import {
  Button,
  TabPane,
  Row,
  Col,
  Modal,
  ModalBody,
  Label
} from "reactstrap";
import Product from "components/shop/Product.js"
import { CartContext } from 'contexts/CartContext';
import { ProductsContext } from 'contexts/ProductsContext';
import FormText from 'reactstrap/lib/FormText';
import { formatNumber } from '../../helpers/utils';
import { isInCart } from '../../helpers/utils';

const ProductsTab = ({ list }) => {
  const { addProduct, cartItems, increase, clearCart } = useContext(CartContext);

  const { shop, slots, reviews } = useContext(ProductsContext);
  const [modal, setModal] = React.useState(false);
  const [product, selectedItem] = React.useState({});
  const [currentDateSlots, setCurrentDateSlots] = React.useState(0);

  const toggleModal = (product) => {
    initProduct(product);
    getCurrentDateSlots();
    setModal(!modal);
    selectedItem(product);
  };

  const initProduct = (product) => {
    product['shopId'] = !!product.shopId ? product.shopId : product['shopId'] = shop.shopId;
    product['shopName'] = !!product.shopName ? product.shopName : product['shopName'] = shop.shopName;
    product['addOn1'] = {};
    product['addOn2'] = {};
    product['addOn3'] = {};
    product['addOn4'] = {};
    product['addOn5'] = {};
    product['addOnsPrice'] = 0;
  }

  const isTheSameShop = product => {
    return !!cartItems.find(item => (item.shopId == product.shopId));
  }

  const addProd = product => {
    if (cartItems.length !== 0 && !isTheSameShop(product)) {
      alert('You have already selected different shop. If you continue your cart and selection will be removed.');
      clearCart();
    }

    if (isInCart(cartItems, product)) {
      increase(product);
    } else {
      addProduct(product);
    }
    toggleModal(product);
  }

  const getCurrentDateSlots = () => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    if (!!slots) {
      const month = slots[yyyy + "-" + mm];
      if (month) {
        const slotAvailable = month[dd].numberOfAvailableSlots;
        setCurrentDateSlots(slotAvailable);
      } else {
        setCurrentDateSlots(0);
      }
    } else {
      setCurrentDateSlots(0);

    }
  }

  return (
    <TabPane tabId="2" id="listing">
      <Row>
        <Col className="ml-auto mr-auto" md="12">
          <Modal isOpen={modal} toggle={toggleModal}>
            <div className="modal-header">
              <button
                aria-label="Close"
                className="close"
                type="button"
                onClick={toggleModal}
              >
                <span aria-hidden={true}>×</span>
              </button>
              <h5 className="modal-title text-center"
                id="exampleModalLabel">
                {product.name}
              </h5>
            </div>
            <img width="100%" src={product.image1} alt="Card image cap" />
            <ModalBody >
              <FormText>{product.description}</FormText>
              <Product slots={slots} product={product} price={product.price} variationList={product.variationList} />
            </ModalBody>
            <div className="modal-footer">
              <Button onClick={() => addProd(product)} className="btn-link" color="danger" type="button">
                Add to Cart
              </Button>
            </div>
          </Modal>
          <ul className="list-unstyled listing">
            {list && list.map((menuItem, index) => {
              return (
                <li key={menuItem.productId} onClick={() => {
                  toggleModal(menuItem);
                }}>
                  <Row>
                    <Col className="ml-auto mr-auto" lg="2" md="4" xs="6">
                      <img
                        alt="..."
                        className="img-fluid"
                        src={menuItem.image1} />
                    </Col>
                    <Col className="ml-auto mr-auto" lg="10" md="8" xs="6">
                      <Button close aria-label="Cancel"
                        onClick={() => {
                          toggleModal(menuItem);
                        }}>
                        <span aria-hidden> + </span>
                      </Button>
                      <h5> {menuItem.name} </h5>
                      <p>
                        {menuItem.description}
                      </p>
                      <Label className="label label-warning"> {formatNumber(menuItem.price)}</Label>
                    </Col>
                  </Row>
                  <hr />
                </li>
              )
            })}
          </ul>
        </Col>
      </Row>
    </TabPane>
  )
}
export default ProductsTab;