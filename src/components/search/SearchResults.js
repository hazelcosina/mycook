import React from "react";

import {
    Card,
    CardBody,
    CardTitle,
    Row,
    Col,
    CardImg
} from "reactstrap";
import { Link } from "react-router-dom";
import ProductsContext from "contexts/ProductsContext.js";

class SearchResults extends React.Component {
    static shops = ProductsContext;
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                {this.props.list && this.props.list.map((shop, index) => {
                    return (
                        <Col key={shop.shopId} md="3" sm="6">
                            <Card className="card-plain">
                                <Link to={{
                                    pathname: '/shop',
                                    state: {
                                        shop: shop
                                    }
                                }}>
                                    <CardBody >
                                        <CardImg className="img-responsive" top width="100%" height="50%" src={shop.restaurantImage} />
                                        <CardTitle tag="h4">{shop.shopName}</CardTitle>
                                        <label className="label label-default mr-1">{shop.address} , {shop.city}</label>
                                    </CardBody>
                                </Link>
                            </Card>
                        </Col>
                    )
                })
                }
            </Row>
        );
    }
}

export default SearchResults