import React, { useContext } from 'react';
import { CartContext } from '../../contexts/CartContext';
import { Button, Col } from "reactstrap";
import { formatNumber } from 'helpers/utils';

const CartTotal = () => {

    const { total, itemCount, clearCart } = useContext(CartContext);

    return (
        <Col md="3" className="p-1">
        <div className="card card-body">
            <p className="mb-1">Total Items:</p>
            <h3 className="m-0 txt-right">{itemCount}</h3>
            <p className="mb-1">Total Payment:</p>
            <h3 className="m-0 txt-right">{formatNumber(total)}</h3>
            <hr color="success"/>
            <div className="text-center">
                <Button href="/pay" color="success">CHECKOUT</Button>
                <Button
                    color="default"
                    onClick={clearCart}>CLEAR</Button>
            </div>
        </div>
    </Col>
     );
}
 
export default CartTotal;