import React, { useContext } from 'react';

import { Button, Card, Form, Input, Container, Row, Col, InputGroup, InputGroupAddon, Modal, ModalBody, FormText } from "reactstrap";
import { CartContext } from 'contexts/CartContext';
import { pay } from 'helpers/APIUtils';
import { AuthContext } from "contexts/AuthContext";
import { useHistory } from "react-router-dom";

const Information = (disabled) => {
    const { total, cartItems, itemCount, clearCart, checkout, handleCheckout } = useContext(CartContext);
    const [modal, setModal] = React.useState(false);
    const [referenceNumber, setReferenceNumber] = React.useState('');
    const [scheduledDate, setScheduledDate] = React.useState('');
    const [isCompleted, setIsCompleted] = React.useState(false);
    const { user } = React.useContext(AuthContext);
    const history = useHistory();

    const toggleModal = (order) => {
        setModal(!modal);
        setReferenceNumber(order.referenceNumber);
        setScheduledDate(order.scheduledDate);

        if (isCompleted) {
            history.push('/myorders');
            clearCart();
        }
    };

    const createOrder = () => {
        const body = { 'cartItems': cartItems, 'totalPayment': total };

        pay(JSON.stringify(body))
            .then(res => {
                setIsCompleted(true);
                toggleModal(res);

            }).catch(error => {
                alert((error && error.message) || 'Oops! Something went wrong. Please try again!');
            });
    }

    return (
        <>

            <div className="filter" />
            <Modal isOpen={modal} toggle={toggleModal}>
                <div className="modal-header">
                    <button
                        aria-label="Close"
                        className="close"
                        type="button"
                        onClick={toggleModal}
                    >
                        <span aria-hidden={true}>×</span>
                    </button>
                    <h5 className="modal-title text-center">
                        Confirmation </h5>
                </div>
                <ModalBody >
                    Your order is now confirmed. We will contact you on your scheduled date {scheduledDate} prior to delivery.
                    Your reference number is #{referenceNumber}.
                    Thank you!
                    <br />
                </ModalBody>
                <div className="modal-footer">
                    <Button onClick={toggleModal} className="btn-link" color="danger" type="button">
                        Close
              </Button>
                </div>
            </Modal>

            <Col className="ml-auto mr-auto m-5" md="4" xs="10">
                <Card className="card card-body mt-3 ml-auto mr-auto bg-light text-dark">
                    <img alt="..." className="mx-auto" width="20%" src="assets/img/mycook-2.png" />
                    <h3 className="mx-auto">Review Information</h3>
                    <label>Contact Person</label>
                    <Input type="text" value={!!user.fullName ? user.fullName : ''} />
                    <Row>
                        <Col lg="6">
                            <label>Contact Number</label>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">+639</InputGroupAddon>
                                <Input type="text" />
                            </InputGroup>
                        </Col>
                        <Col lg="6">
                            <label>Delivery Date</label>
                            <Input type="text" value={user.contactNumber} />
                        </Col>
                    </Row>
                    <label>Delivery Address</label>
                    <Input type="text" value={user.deliveryAddress} />
                    <label>Other Notes</label>
                    <Input type="textarea" />
                    <label>Total Amount</label>
                    <Input disabled type="text" value={total} />
                    <br />
                    <Button onClick={createOrder} block className="btn-round" color="success">
                        Complete
                  </Button>
                    <FormText color="danger">Note: Please carefully review contact information upon completion. </FormText>
                    <br />
                </Card>
            </Col>
        </>
    );
}

export default Information;
