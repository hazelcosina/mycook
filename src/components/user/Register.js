import React from "react";

import { Button, Card, Form, Input, Container, Row, Col, InputGroup, InputGroupAddon } from "reactstrap";

function Register() {

  return (
    <>
      <div className="filter" />
      <Col className="ml-auto mr-auto" lg="4">
        <Card className="card card-body mt-3 ml-auto mr-auto bg-light text-dark">
          <h3 className="mx-auto">Create Account</h3>
          <small className="mx-auto">Or Log In via</small>
          <div className="social-line text-center">
            <Button
              className="btn-neutral btn-just-icon mr-1"
              color="facebook"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <i className="fa fa-facebook-square" />
            </Button>
            <Button
              className="btn-neutral btn-just-icon mr-1"
              color="google"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <i className="fa fa-google-plus" />
            </Button>
          </div>

          <br />

          <Form >
            <label>Email</label>
            <Input placeholder="Email" type="text" />
            <label>Password</label>
            <Input placeholder="Password" type="password" />
            <label>Contact Number</label>
            <InputGroup>
              <InputGroupAddon addonType="prepend">+639</InputGroupAddon>
              <Input type="text" />
            </InputGroup>
            <label>Delivery Address</label>
            <Input type="text" />
            <br />

            <Button block className="btn-round" color="success">
              Sign Up
                  </Button>
          </Form>
          <Button
            className="btn-link text-muted"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            Already have an account?
                  </Button>
        </Card>
      </Col>
    </>
  );
}

export default Register;
