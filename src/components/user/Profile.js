import React from "react";
import { AuthContext } from "contexts/AuthContext";

import { Button, Card, Form, Input, Container, Row, Col, InputGroup, InputGroupAddon } from "reactstrap";

const Profile = (disabled) => {
    const { user } = React.useContext(AuthContext);

    return (
        <>
            <div className="filter" />
            <Col className="ml-auto mr-auto" lg="8" >
                <Card className="card card-body border-0 mt-3 ml-auto mr-auto bg-light text-dark">
                    <h3 className="mx-auto"> <i className="nc-icon nc-alert-circle-i align-middle" /> &nbsp; Account </h3>
                    <label>Full Name</label>
                    <Input type="text" value={!!user.fullName ? user.fullName : ''} />
                    <Row>
                        <Col lg="6">
                            <label>Contact Number</label>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">+639</InputGroupAddon>
                                <Input type="text" value={user.contactNumber} />
                            </InputGroup>
                        </Col>
                        <Col lg="6">
                            <label>Email Address</label>
                            <Input type="text" disabled value={!!user.emailAddress ? user.emailAddress : ''} />
                        </Col>
                    </Row>
                    <label>Delivery Address</label>
                    <Row>
                        <Col lg="6"><Input type="text" placeholder="House #, Street" /></Col>
                        <Col lg="3"><Input type="text" placeholder="Brgy." /></Col>
                        <Col lg="3"><Input type="text" placeholder="City/Municipality" /></Col>
                    </Row>
                    <Row>
                        <Col lg="6">
                            <label>Current Password</label>
                            <Input type="password" />
                        </Col>
                        <Col lg="6">
                            <label>New Password</label>
                            <Input type="password" />
                        </Col>
                    </Row>
                    <br/>
                    <Button className="btn-round" color="outline-success">
                        Update
                  </Button>
                </Card>
            </Col>
            <Col className="ml-auto mr-auto" lg="8" >
                <Card className="card card-body border-0 mt-3 ml-auto mr-auto bg-light text-dark">
                    <h3 className="mx-auto"> <i className="nc-icon nc-credit-card align-middle "/> &nbsp; Payments </h3>
                    <label>Payment 1</label>
                    <Row>
                        <Col lg="6"><Input type="text" placeholder="13-Digit Credit Card Number" /></Col>
                        <Col lg="3"><Input type="text" placeholder="01/01" /></Col>
                        <Col lg="3"><Input type="text" placeholder="000" value={!!user.ccLast4digit ? user.ccLast4digit : ''} /></Col>
                    </Row>
                    <label>Payment 2</label>
                    <Row>
                        <Col lg="6"><Input type="text" placeholder="13-Digit Credit Card Number" /></Col>
                        <Col lg="3"><Input type="text" placeholder="Expiry Date" /></Col>
                        <Col lg="3"><Input type="text" placeholder="CC" value={!!user.ccLast4digit ? user.ccLast4digit : ''} /></Col>
                    </Row>
                    <br />
                    <Button className="btn-round" color="outline-success">
                        Save
                  </Button>
                </Card>

            </Col>
        </>
    );
}

export default Profile;
