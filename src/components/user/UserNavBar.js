import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import {
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
} from "reactstrap";
import { AuthContext } from "contexts/AuthContext";
import { CartContext } from 'contexts/CartContext';

function UserNavBar() {
  const [navbarColor, setNavbarColor] = React.useState("navbar-success");
  const [navbarCollapse, setNavbarCollapse] = React.useState(false);
  const { isAuthUser, user, logout } = React.useContext(AuthContext);
  const { itemCount } = React.useContext(CartContext);

  const toggleNavbarCollapse = () => {
    setNavbarCollapse(!navbarCollapse);
    document.documentElement.classList.toggle("nav-open");
  };

  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 299 ||
        document.body.scrollTop > 299
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 300 ||
        document.body.scrollTop < 300
      ) {
        setNavbarColor("navbar-success");
      }
    };

    window.addEventListener("scroll", updateNavbarColor);

    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });
  return (
    <Navbar
      className={classnames("fixed-top", navbarColor)}
      color-on-scroll="300"
      expand="lg"
    >
      <Container>
        <div className="navbar-translate">
          <NavbarBrand
            data-placement="bottom"
            to="/"
            title="Coded by Creative Tim"
            tag={Link}
          >
            <img alt="..." className="creative-tim-logo" width="10%" src="assets/img/mycook-2.png" />
            MyCook

          </NavbarBrand>


          <button
            aria-expanded={navbarCollapse}
            className={classnames("navbar-toggler navbar-toggler", {
              toggled: navbarCollapse,
            })}
            onClick={toggleNavbarCollapse}
          >
            <span className="navbar-toggler-bar bar1" />
            <span className="navbar-toggler-bar bar2" />
            <span className="navbar-toggler-bar bar3" />
          </button>


        </div>
        <Collapse
          className="justify-content-end"
          navbar
          isOpen={navbarCollapse}
        >
          <Nav navbar>
          <NavItem>
              <NavLink to="/cart" tag={Link}>
                <i className="nc-icon nc-cart-simple" /> ( {itemCount} ) CART
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/" onClick={logout} tag={Link}>
                <i className="nc-icon nc-lock-circle-open" /> LOG OUT
                </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
}

export default UserNavBar;
