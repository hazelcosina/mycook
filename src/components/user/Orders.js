import React from "react";
import { AuthContext } from "contexts/AuthContext";
import { formatNumber } from '../../helpers/utils';

import { Button, Card, Label, Form, Input, Container, Row, Col, InputGroup, InputGroupAddon } from "reactstrap";
import { login, getUserOrders } from 'helpers/APIUtils';
import FormText from "reactstrap/lib/FormText";

const Orders = (disabled) => {

    const { user } = React.useContext(AuthContext);
    const [activeOrders, setActiveOrders] = React.useState([]);
    const [completedOrders, setCompletedOrders] = React.useState([]);
    const [cancelledOrders, setCancelledOrders] = React.useState([]);

    React.useEffect(() => {
        getUserOrders().then(res => {
            if (res) {
                const activeOrders = res.filter(order => (order.status === 'active'));
                setActiveOrders(activeOrders);

                const completedOrders = res.filter(order => (order.status === 'completed'));
                setCompletedOrders(completedOrders);

                const cancelledOrders = res.filter(order => (order.status === 'cancelled'));
                setCancelledOrders(cancelledOrders);
            }
        },
            error => {
                alert(error.message);
            });
    }, []);


    return (
        <>
            <div className="filter" />
                <Col  lg="6" >
                <Card className="card card-body border-0 mt-3 ml-auto mr-auto bg-light text-dark">
                    <h3 className="mx-auto"> <i className="nc-icon nc-delivery-fast align-middle"/> &nbsp; For Delivery  </h3>
                    <hr />

                    <ul className="list-unstyled listing">
                        
                        {activeOrders && activeOrders.map((order) => {
                            return (
                                <li>
                                    <Row>
                                        <Col className="ml-auto mr-auto" lg="3" md="3" xs="3">
                                            <img
                                                alt="..."
                                                className="img-rounded img-responsive"
                                                src={order.shopImage} />
                                        </Col>
                                        <Col className="ml-auto mr-auto" lg="6" md="6" xs="6">
                                            <h5> {order.shopName}</h5>

                                            {order.orderedItems && order.orderedItems.map((orderItem, i) => {
                                                const products = Object.keys(orderItem);

                                                return (
                                                    <li>
                                                        {products.map((product) => {
                                                            const addOnKeys = Object.keys(orderItem[product]).filter(product => product !== 'quantity');

                                                            return (
                                                                <small>
                                                                    <li>{product} x {orderItem[product]['quantity']}                                                                    </li>

                                                                    {addOnKeys.map((variation) => {
                                                                        const option = orderItem[product][variation];
                                                                        return <>
                                                                            {variation}:{option} &nbsp;
                                                                        </>
                                                                    })}
                                                                </small>

                                                            )
                                                        })}
                                                    </li>
                                                )
                                            })}
                                            <br />
                                        </Col>
                                        <Col className="ml-auto mr-auto align-left" lg="3" md="3" xs="3">
                                            <Label className="label label-default">{order.status}</Label>
                                            <br />
                                            <Label className="label label-warning">{formatNumber(order.totalPayment)}</Label>
                                            <br />
                                            <Label className="label text-muted">{order.scheduledDate} @ {order.scheduledTime}</Label>
                                            <br />

                                        </Col>
                                    </Row>
                                    <hr />
                                </li>
                            )
                        })}
                        <></>
                        {activeOrders.length == 0 ?
                        <FormText className="text-center">
                        No upcoming deliveries
                        </FormText>
                        :
                        <></>
                        }
                    
                    </ul>
                </Card>
                </Col>
                <Col md="6">
                <Card className="card card-body border-0 mt-3 ml-auto mr-auto  text-dark">
                    <h3 className="mx-auto"><i className="nc-icon nc-check-2 align-middle"/> &nbsp;  Completed </h3>
                    <hr />
                    <ul className="list-unstyled listing">
                        
                        {completedOrders && completedOrders.map((order) => {
                            return (
                                <li>
                                    <Row>
                                        <Col className="ml-auto mr-auto" lg="3" md="3" xs="3">
                                            <img
                                                alt="..."
                                                className="img-rounded img-responsive"
                                                src={order.shopImage} />
                                        </Col>
                                        <Col className="ml-auto mr-auto" lg="6" md="6" xs="6">
                                            <h5> {order.shopName}</h5>

                                            {order.orderedItems && order.orderedItems.map((orderItem, i) => {
                                                const products = Object.keys(orderItem);

                                                return (
                                                    <li>
                                                        {products.map((product) => {
                                                            const addOnKeys = Object.keys(orderItem[product]).filter(product => product !== 'quantity');

                                                            return (
                                                                <small>
                                                                    <li>{product} x {orderItem[product]['quantity']}                                                                    </li>

                                                                    {addOnKeys.map((variation) => {
                                                                        const option = orderItem[product][variation];
                                                                        return <>
                                                                            {variation}:{option} &nbsp;
                                                                        </>
                                                                    })}
                                                                </small>

                                                            )
                                                        })}
                                                    </li>
                                                )
                                            })}
                                            <br />
                                        </Col>
                                        <Col className="ml-auto mr-auto align-left" lg="3" md="3" xs="3">
                                            <Label className="label label-default">{order.status}</Label>
                                            <br />
                                            <Label className="label label-warning">{formatNumber(order.totalPayment)}</Label>
                                            <br />
                                            <Label className="label text-muted">{order.scheduledDate} @ {order.scheduledTime}</Label>
                                            <br />

                                        </Col>
                                    </Row>
                                    <hr />
                                </li>
                            )
                        })}
                        <></>
                        {completedOrders.length == 0 ?
                        <FormText className="text-center">
                        No upcoming deliveries
                        </FormText>
                        :
                        <></>
                        }
                    
                    </ul>
                </Card>
                </Col>
        </>
    );
}

export default Orders;
