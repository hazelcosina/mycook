import React from "react";
import { Container, FormGroup, Input, Col, Row } from "reactstrap";

function IndexHeader() {
  return (
    <>
      <div
        className="page-header section-dark"
        style={{
          backgroundImage:
            "url(" + require("assets/img/landing/food2.jpg") + ")",
        }}
      >
        <div className="filter" />
        <div className="content-center">
          <Container>
            <div className="title-brand">
              <h1 >Find your nearest cook</h1>
              <div className="fog-low">
                <img alt="..." src={require("assets/img/fog-low.png")} />
              </div>
              <div className="fog-low right">
                <img alt="..." src={require("assets/img/fog-low.png")} />
              </div>
            </div>
            <Row>
              <Col lg="12" md="12" sm="3">
                <FormGroup>
                  <Input placeholder="Enter your location" type="text" />
                </FormGroup>
              </Col>
            </Row>

            <h2 className="presentation-subtitle text-center">
              Let us do the cooking for you! Order now
            </h2>
          </Container>
        </div>
        <div
          className="moving-clouds"
          style={{
            backgroundImage: "url(" + require("assets/img/clouds.png") + ")",
          }}
        />
      </div>
    </>
  );
}

export default IndexHeader;
