import React from "react";
import { Button, CardBody, Container, InputGroup, InputGroupAddon, Input, Col, Modal, ModalBody, FormText } from "reactstrap";
import FormGroup from "reactstrap/lib/FormGroup";
import Calendar from "./Calendar";
var DatePicker = require("reactstrap-date-picker");

const LandingPageHeader = () => {
  let pageHeader = React.createRef();
  let currentDate = new Date().toISOString();

  const [modal, setModal] = React.useState(false);
  const [value, setValue] = React.useState(currentDate);

  const toggleModal = (product) => {
    setModal(!modal);
  };
  React.useEffect(() => {
    if (window.innerWidth < 991) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 3;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });

  const handleDateChange = (value, formattedValue) => {
    setValue(value);
  }

  return (

    <>
      <div
        style={{
          backgroundImage:
            "url(" + require("assets/img/landing/food2.jpg") + ")",
        }}
        className="page-header"
        data-parallax={true}
        ref={pageHeader}
      >
        <div className="filter" />
        <Container>
          <Modal isOpen={modal} toggle={toggleModal}>
            <div className="modal-header">
              <button
                aria-label="Close"
                className="close"
                type="button"
                onClick={toggleModal}
              >
                <span aria-hidden={true}>×</span>
              </button>
              <h5 className="modal-title text-center"
                id="modal-label">
                Welcome to MyCook!
              </h5>
            </div>
            <ModalBody >
              <p>Thank you for taking the tour with us.</p>
              <br />
            </ModalBody>
            <div className="modal-footer">
              <Button href="/list" className="btn-link" color="danger" type="button">
                Continue
              </Button>
            </div>
          </Modal>
          <div className="motto">
            <Col className="m-auto" sm="12" lg="5" md="8">
              <h2 className="font-weight-bold">Food prepared just for you</h2>
              <br />
              <Input placeholder="Cake, Lechon Belly.." />
              <InputGroup>
                <Calendar />
                <InputGroupAddon addonType="append">
                  <Button href="/list" color="success">Find Nearby</Button>
                </InputGroupAddon>
              </InputGroup>
            </Col>
          </div>
        </Container>
        <div
          className="moving-clouds"
          style={{
            backgroundImage: "url(" + require("assets/img/clouds.png") + ")",
          }}
        />
      </div>
    </>
  );
}

export default LandingPageHeader;
