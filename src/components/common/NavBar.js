import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import {
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  ButtonGroup,
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import { CartContext } from 'contexts/CartContext';
import { AuthContext } from "contexts/AuthContext";

function NavBar() {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [navbarCollapse, setNavbarCollapse] = React.useState(false);
  const { itemCount } = React.useContext(CartContext);
  const { isAuthUser, user, logout } = React.useContext(AuthContext);

  const toggleNavbarCollapse = () => {
    setNavbarCollapse(!navbarCollapse);
    document.documentElement.classList.toggle("nav-open");
  };

  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 299 ||
        document.body.scrollTop > 299
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 300 ||
        document.body.scrollTop < 300
      ) {
        setNavbarColor("navbar-transparent");
      }
    };

    window.addEventListener("scroll", updateNavbarColor);

    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });
  return (
    <Navbar
      className={classnames("fixed-top", navbarColor)}
      color-on-scroll="300"
      expand="lg"
    >
      <Container>
        <div className="navbar-translate">
          <NavbarBrand
            data-placement="bottom"
            to="/"
            title="Coded by Creative Tim"
            tag={Link}
          >
            <img alt="..." className="creative-tim-logo" width="10%" src="assets/img/mycook-2.png" />
            MyCook
          </NavbarBrand>
          <ButtonGroup>

            <button
              aria-expanded={navbarCollapse}
              className={classnames("navbar-toggler navbar-toggler", {
                toggled: navbarCollapse,
              })}
              onClick={toggleNavbarCollapse}
            >
              <span className="navbar-toggler-bar bar1" />
              <span className="navbar-toggler-bar bar2" />
              <span className="navbar-toggler-bar bar3" />
            </button>

          </ButtonGroup>

        </div>
        <Collapse
          className="justify-content-end"
          navbar
          isOpen={navbarCollapse}
        >
          <Nav navbar>
          {isAuthUser ?
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle caret nav>
                    <i className="nc-icon nc-circle-10" /> {!!user.fullName ? user.fullName : user.username}
                </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem href="/myorders"> 
                      My Orders
                  </DropdownItem>
                    <DropdownItem href="/myaccount">
                      Settings
                  </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem onClick={logout}>
                      Log out
                  </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              :
              <NavItem>
                <NavLink to="/login" tag={Link}>
                  <i className="nc-icon nc-circle-10" /> LOG IN
            </NavLink>
              </NavItem>
            }
            <NavItem>
              <NavLink to="/cart" tag={Link}>
                <i className="nc-icon nc-cart-simple" /> ( {itemCount} ) CART
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
