import React from "react";

import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import LandingPage from "views/LandingPage.js";
import ShopProfilePage from "views/ShopProfilePage.js";
import SearchResultsPage from "views/SearchResultsPage.js";
import PaymentsPage from "views/PaymentsPage.js";
import CartPage from "views/CartPage.js"
import CartContextProvider from "contexts/CartContext";
import MyOrdersPage from "views/MyOrdersPage";
import UserProfilePage from "views/UserProfilePage";
import LoginPage from "views/LoginPage";
import AuthRoute from "./AuthRoute";
import AuthContextProvider from "contexts/AuthContext";
import RegisterPage from "views/RegisterPage";
import OAuth2RedirectHandler from "components/user/OAuth2RedirectHandler";


function App() {

    return (
        <CartContextProvider>
            <AuthContextProvider>
                <BrowserRouter>
                    <Switch>
                        <Route
                            path="/search"
                            render={(props) => <LandingPage {...props} />}
                        />
                        <Route
                            path="/shop"
                            render={(props) => <ShopProfilePage {...props} />}
                        />
                        <Route
                            path="/list"
                            render={(props) => <SearchResultsPage {...props} />}
                        />
                        <Route
                            path="/cart"
                            render={(props) => <CartPage {...props} />}
                        />
                        <Route
                            path="/login"
                            render={(props) => <LoginPage {...props} />}
                        />
                         <Route
                            path="/register"
                            render={(props) => <RegisterPage {...props} />}
                        />
                         <Route 
                            path="/oauth2/redirect"
                            render={(props) => <OAuth2RedirectHandler {...props} />}
                                />
                        <AuthRoute path="/pay" render={PaymentsPage} type="private" />
                        <AuthRoute path="/myorders" render={MyOrdersPage} type="private" />
                        <AuthRoute path="/myaccount" render={UserProfilePage} type="private" />
                        <Redirect to="/search" />
                    </Switch>
                </BrowserRouter>
            </AuthContextProvider>
        </CartContextProvider>
    )
}
export default App;
