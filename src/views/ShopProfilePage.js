import React from "react";
import ProfilePageHeader from "components/shop/ProfilePageHeader.js";
import ProfilePageSection from "components/shop/ProfilePageSection.js";
import NavBar from "components/common/NavBar.js";
import Footer from "components/common/Footer.js";
import ProductsContext from "contexts/ProductsContext";
import { getSlots } from 'helpers/APIUtils';
import { useParams
} from "react-router-dom";

class ShopProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: {
        numberOfItems: 0,
        totalPrice: 0
      },
      slots: {},
      reviews: {}
    }
  }

  componentDidMount() {
    const shopId = this.props.location.state.shop.shopId;
    getSlots(shopId).then(res => {
      this.setState({
        slots: res.slots,
        reviews: res.customerReviews
      })
    }).catch(err => {
      alert(err);
    })
  }

  render() {
    
    const shop = this.props.location.state.shop;
    const slots = this.state.slots;
    const reviews = this.state.reviews;
    const contextValues = { shop, slots, reviews, ...this.state }

    return (
      <>
        <NavBar />
        <ProductsContext.Provider value={contextValues}>
          <ProfilePageHeader />
          <ProfilePageSection />
        </ProductsContext.Provider>
        <Footer />
      </>
    );
  }
}
export default ShopProfilePage;