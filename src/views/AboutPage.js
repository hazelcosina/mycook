import React from "react";

import LandingNavBar from "components/common/LandingNavBar.js";
import Footer from "components/common/Footer.js";

function AboutPage() {
  document.documentElement.classList.remove("nav-open");

  return (
    <>
      <LandingNavBar />
      <Footer />
    </>
  );
}

export default AboutPage;
