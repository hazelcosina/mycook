import React from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
} from "reactstrap";
import LandingNavBar from "components/common/LandingNavBar.js";
import LandingPageHeader from "components/common/LandingPageHeader.js";
import Footer from "components/common/Footer.js";
import FormText from "reactstrap/lib/FormText";

function LandingPage() {
  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    document.body.classList.add("profile-page");
    return function cleanup() {
      document.body.classList.remove("profile-page");
    };
  });
  return (
    <>
      <LandingNavBar />
      <LandingPageHeader />
      <div className="main">
        <div className="section text-center">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" md="10">
                <p>For the home cooks, freelance chefs, food caterers, and to all of the foodies out there</p>
                <img alt="..." className="img-fluid" src={require("assets/img/use-case-3.PNG")}  />
                <h2 className="title">What's in it for us?</h2>
                <h5>
                  MyCook connects users to food merchants that require advance preparation and reservation. This platform lets us plan occasions ahead of time quick and easy.
                </h5>

              </Col>
            </Row>
          </Container>
        </div>

        <div className="section section-dark landing-section text-white ">
          <Container>
            <Row>
              <Col md="6">
                <h2 className="text-left">Become one of our initial partners
                <small > and we'll waive the commision fee for you</small>
                </h2> <br/>
                <Button className="btn-fill align-center" color="success" size="lg"> Message Us </Button>
              </Col>
              <Col className="ml-auto mr-auto" md="6"></Col>
              
            </Row>
          </Container>
        </div>
        <div className="section landing-section  ">
          <Container>
           
            <Row>
            <Col className="ml-auto mr-auto" md="6"></Col>

            <Col className="text-right" md="6">
                <h2 className="">Apply as a Rider
                <small > and earn extra income while taking hold of your time</small>
                </h2> <br/>
                <Button className="btn-fill" color="success" size="lg"> Apply here </Button>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default LandingPage;
