import React from "react";

import {
  Container, Col
} from "reactstrap";

import SearchNavBar from "components/search/SearchNavBar.js";
import Footer from "components/common/Footer.js";
import SearchBar from "components/search/SearchBar";
import SearchResults from "components/search/SearchResults.js"
import {  searchAll } from 'helpers/APIUtils';


const SearchResultsPage = (props) => {
  const [shops, setShops] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    searchAll().then((data) => {
      setShops(data);
    })
      .then(() => {
        setLoading(false);
      }).catch(err => {
        console.log(err.log);
      }).catch(err => {
        console.log(err.log);
      });
  }, []);

  return (
    <>
      <SearchNavBar />
      <div className="main">
        <br />
        <div className="section section-dark text-center">

          <Container>
            <SearchBar />
            {loading ?
              <Col sm="12" md={{ size: 6, offset: 3 }}>
                <br />
                <Container>
                  <h2 className="text-white">Getting Ready </h2>
                  <img width="10%" alt="Loading" className="img-responsive" src="assets\img\examples\loading.gif"></img>
                  <img width="10%" alt="Loading" className="img-responsive" src="assets\img\examples\loading.gif"></img>
                  <img width="10%" alt="Loading" className="img-responsive" src="assets\img\examples\loading.gif"></img>
                  <img width="10%" alt="Loading" className="img-responsive" src="assets\img\examples\loading.gif"></img>
                </Container>
              </Col>
              :
              <SearchResults list={shops} />
            }
          </Container>
        </div>
      </div>
      <Footer />
    </>
  );
}
export default SearchResultsPage;

