import React, { useContext, useState } from 'react';
import CartProducts from 'components/cart/CartProducts';
import { CartContext } from 'contexts/CartContext';
import { Link } from 'react-router-dom';
import NavBar from "components/common/NavBar.js";
import { Container, Button, Col, Modal, ModalBody, FormText } from "reactstrap";
import Footer from "components/common/Footer.js";
import CartTotal from "components/cart/CartTotal.js"
import SearchNavBar from 'components/search/SearchNavBar';

const CartPage = () => {

    const { total, cartItems, itemCount, clearCart, checkout, handleCheckout } = useContext(CartContext);
    const [modal, setModal] = useState(false);

    document.documentElement.classList.remove("nav-open");

    const toggleModal = (product) => {
        setModal(!modal);
    };

    return (
        <>
            <SearchNavBar />
            <div className="main">
                <br />
                <div className="section">
                    <Container>
                        <div >
                            <div className="row no-gutters p-1">
                                <Col md="9" className="p-1">
                                    {
                                        cartItems.length > 0 && !checkout ?
                                            <ul className="list-unstyled listing">
                                                <CartProducts />
                                            </ul> :
                                            <>
                                                <h2 class=" text-center">
                                                    Your cart is empty </h2>
                                                <center>
                                                    <br /><br />
                                                    <Link to="/search" className="btn btn-info">ORDER FIRST</Link>
                                                </center>
                                            </>
                                    }
                                </Col>
                                {
                                    cartItems.length > 0 &&
                                    <CartTotal />
                                }
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
            < Footer />
        </>
    );
}

export default CartPage;