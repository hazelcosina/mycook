import React from "react";

import SearchNavBar from "components/search/SearchNavBar.js";
import Footer from "components/common/Footer.js";
import Orders from "components/user/Orders.js";

import {  Container, Row } from "reactstrap";

function MyOrdersPage() {
    document.documentElement.classList.remove("nav-open");
    return (
        <>
            <SearchNavBar />
            <div className="main">
                <br />
                <div className="section ">
                    <Container>
                        <Row>
                            <Orders />
                        </Row>
                    </Container>
                </div>

            </div>
            <Footer />
        </>
    );
}

export default MyOrdersPage;
