import React, { useState, useContext } from "react";
import { signup } from 'helpers/APIUtils';
import { FACEBOOK_AUTH_URL, GOOGLE_AUTH_URL} from '../constants';
import { Button, Card, Form, Input, InputGroup, InputGroupAddon, Col } from "reactstrap";


const RegisterPage = (props) => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [deliveryAddress, setDeliveryAddress] = useState("");
    const [contactNumber, setContactNumber] = useState("");

    const handleEmailAddressChange = event => {
        setEmail(event.target.value);
    }

    const handlePasswordChange = event => {
        setPassword(event.target.value);
    }

    const handleDeliveryAddressChange = event => {
        setDeliveryAddress(event.target.value);
    }

    const handleContactNumberChange = event => {
        setContactNumber(event.target.value);
    }

    const handleFormSubmit = event => {
        event.preventDefault();

        const user_object = {
            email: email,
            password: password,
            deliveryAddress: deliveryAddress,
            contactNumber: contactNumber
        };

        signup(user_object)
            .then(response => {
                alert("You're successfully registered. Verify contact number");
            }).catch(error => {
                alert((error && error.message) || 'Oops! Something went wrong. Please try again!');
            });
    }


    document.documentElement.classList.remove("nav-open");
    React.useEffect(() => {
        document.body.classList.add("profile-page");
        return function cleanup() {
            document.body.classList.remove("profile-page");
        };
    });

    return (
        <>
            <div className="main ">
                <br />
                <div className="section">
                    <Col className="m-auto" sm="12" lg="8" md="8">
                        <div className="filter" />
                        <Col className="ml-auto mr-auto" lg="4">
                            <Card className="card card-body mt-3 ml-auto mr-auto bg-light text-dark">
                                <h3 className="mx-auto">Create Account</h3>
                                <small className="mx-auto">Or Log In via</small>
                                <div className="social-line text-center">
                                    <Button
                                        className="btn-neutral btn-just-icon mr-1"
                                        color="facebook"
                                        href={FACEBOOK_AUTH_URL}
                                    >
                                        <i className="fa fa-facebook-square" />
                                    </Button>
                                    <Button
                                        className="btn-neutral btn-just-icon mr-1"
                                        color="google"
                                        href={GOOGLE_AUTH_URL}
                                    >
                                        <i className="fa fa-google-plus" />
                                    </Button>
                                </div>

                                <br />

                                <Form >
                                    <label>Email</label>
                                    <Input placeholder="Email" type="text" onChange={handleEmailAddressChange} />
                                    <label>Password</label>
                                    <Input placeholder="Password" type="password" onChange={handlePasswordChange} />
                                    <label>Contact Number</label>
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">+639</InputGroupAddon>
                                        <Input type="text" onChange={handleContactNumberChange} />
                                    </InputGroup>
                                    <label>Delivery Address</label>
                                    <Input type="text" onChange={handleDeliveryAddressChange} />
                                    <br />

                                    <Button onClick={handleFormSubmit} block className="btn-round" color="success">  Sign Up  </Button>
                                </Form>
                                <Button
                                    className="btn-link text-muted"
                                    href="/login"
                                > Already have an account? </Button>
                            </Card>
                        </Col>
                    </Col>
                </div>

            </div>
        </>
    );
}

export default RegisterPage;
