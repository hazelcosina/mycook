import React from "react";
import { useHistory } from "react-router-dom";
import { login, getCurrentUser } from 'helpers/APIUtils';
import { GOOGLE_AUTH_URL, OAUTH2_REDIRECT_URI, FACEBOOK_AUTH_URL, GITHUB_AUTH_URL, ACCESS_TOKEN } from '../constants';

import { Button, Card, Form, Input, InputGroup, Container, Row, Col } from "reactstrap";
import { AuthContext } from "contexts/AuthContext";

const LoginPage = (props) => {
  const { success, isAuthUser, error, setLoader, logout } = React.useContext(AuthContext);
  const [userName, setUserName] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [oAuthRedirect, setoAuthRedirect] = React.useState("");
  const history = useHistory();


  const handleUserNameChange = event => {
    setUserName(event.target.value);
  }

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  }

  const handleFormSubmit = event => {
    event.preventDefault();

    const user_object = {
      email: userName,
      username: userName,
      role: 'USER',
      password: password
    };

    login(user_object)
    .then(response => {
        localStorage.setItem(ACCESS_TOKEN, response.accessToken);
        handleDashboard();
    }).catch(error => {
        alert((error && error.message) || 'Oops! Something went wrong. Please try again!');
    });
  }

  const handleDashboard = () => {
    let redirect = !!props.location.state ? props.location.state.from : '/';
     getCurrentUser().then(response => {
       success(response);
       history.push(redirect);
     })
  }


  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    let redirect = !!props.location.state ? props.location.state.from : '/';

    if(isAuthUser){
      history.push(redirect);
    }
  
    setoAuthRedirect(redirect);
    document.body.classList.add("profile-page");
    return function cleanup() {
      document.body.classList.remove("profile-page");
    };
  });

  return (
    <>
      <div className="main ">
        <br />
        <div className="section">
          <Col className="m-auto" sm="12" lg="8" md="8">
            <div className="filter" />
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card card-body mt-3 ml-auto mr-auto bg-light text-dark">
                <img alt="..." className="mx-auto" width="50%" src="assets/img/mycook-2.png" />
                <small className="mx-auto">Login via Google/Facebook</small>
                <div className="social-line text-center">
                  <Button
                    className="btn-neutral btn-just-icon mr-1"
                    size="lg"
                    color="facebook"
                    href={FACEBOOK_AUTH_URL + oAuthRedirect}
                  >
                    <i className="fa fa-facebook-square" />
                  </Button>
                  <Button
                    className="btn-neutral btn-just-icon"
                    size="lg"
                    color="google"
                    href={GOOGLE_AUTH_URL + oAuthRedirect}
                  >
                    <i className="fa fa-google-plus" />
                  </Button>
                </div>
                <small className="mx-auto">or by email address</small>
                <br />
                <Form >
                  <label>Username</label>
                  <Input placeholder="Username" name="username" type="text" onChange={handleUserNameChange} />
                  <label>Password</label>
                  <Input placeholder="Password" name="password" type="password" onChange={handlePasswordChange} />
                  <br />
                  <Button onClick={handleFormSubmit} block className="btn-round" color="success">
                    Log In
                  </Button>
                </Form>
                <Button
                  className="btn-link text-muted"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                >
                  Forgot password?
                  </Button>
                <Button
                  className="btn-link text-muted"
                  href="/register"
                >
                  Create Account
                  </Button>
              </Card>              
            </Col>
          </Col>
        </div>

      </div>
    </>
  );
}

export default LoginPage;
