import { API_BASE_URL, ACCESS_TOKEN } from '../constants';

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        );
};

export function getCurrentUser() {
    if (!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.");
    }

    return request({
        url: API_BASE_URL + "/user/me",
        method: 'GET'
    });
}

export function login(loginRequest) {
    return request({
        url: API_BASE_URL + "/auth/login",
        method: 'POST',
        body: JSON.stringify(loginRequest)
    });
}

export function signup(signupRequest) {
    return request({
        url: API_BASE_URL + "/auth/signup",
        method: 'POST',
        body: JSON.stringify(signupRequest)
    });
}

export function searchAll(){
    return request({
        url: API_BASE_URL + "/api/shops/all",
        method: 'GET'
    });
}

export function getUserOrders() {
    return request({
        url: API_BASE_URL + "/order/get",
        method: 'GET'
    });
}

export function getShopById(shopId){
    return request({
        url: API_BASE_URL + "/api/shops/get/" + shopId,
        method: 'GET'
    });
}
export function getSlots(shopId){
    return request({
        url: API_BASE_URL + "/reservation/slot/" + shopId,
        method: 'GET'
    });
}

export function pay(body) {
    return request({
        url: API_BASE_URL + '/order/new',
        method: 'POST',
        body: body
    });
}